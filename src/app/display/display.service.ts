import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class DisplayService {

  constructor(private httpClient: HttpClient) { 

  }

  public getData(): Observable<any>{
     return this.httpClient.get("http://localhost:4200/assets/sample_data.json");
  }
}
