import { Component, OnInit } from '@angular/core';
import { DisplayService } from './display.service';
import { PagerService } from './pager.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {

  dataArray1:any=[];
  records:number;
  start:any=0;
  numberOfPages: any=0;
  pagedItems: any[];
  pager: any = {};

  constructor(private displayService : DisplayService, private pagerService: PagerService) { }

  ngOnInit() {
    this.getData();
    
  }
  getData(){
    this.displayService.getData().subscribe(data=>{
      this.pagedItems = data;
      this.dataArray1 = data;
      console.log(this.dataArray1.length);
      this.setPage(1);
    });
  }

  changeRecords(){
   this.records = this.records? this.records:10;
 this.displayService.getData().subscribe(data=>{
 // this.pagedItems = data.slice(0, this.records);
  this.numberOfPages = (this.dataArray1.length)/Number(this.records);
  this.pager.endIndex = this.numberOfPages;
  this.setPage(1);
});
    
  }

  setPage(page: number) {
    if (page < 1) {
        return;
    }

    this.pager = this.pagerService.getPager(this.dataArray1.length, page, this.records);
   this.pagedItems = this.dataArray1.slice(this.pager.startIndex, this.pager.endIndex + 1);
}
postData(id){

}

}
